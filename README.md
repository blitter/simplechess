SimpleChess
===========

This is a simple chess demo I wrote as an exercise, built on SDL2 and OpenGL 2. Available under the 3-clause BSD license.

How to Use
----------

1. Hover over a piece of your color.
2. Click on that piece to select it.
3. Click on a highlighted tile to move that piece.
4. Alternate between players.
5. Click the "Reset" button at the top of the window to create a fresh board.

Features
--------

- It's in 3D.
- The pieces "slide" between tiles.
- Hovering over a piece you don't control will highlight the tile in red as a visual indicator.
- A white arrow points at the player's current score as another visual indicator.
- Pieces that can be captured by the piece hovered over are highlighted in yellow.

Build Process
-------------

This demo was put together using Xcode for Snow Leopard. It requires the SDL2, SDL2_ttf, SDL2_mixer, and GLEW frameworks installed in order to build. It also depends on tinyobjloader ([https://github.com/syoyo/tinyobjloader](https://github.com/syoyo/tinyobjloader)); a snapshot of which is included.

Have fun.

Keith Kaisershot

3-14-17
