//
//  pt3.h
//
//  Created by Keith Kaisershot on 10/3/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef pt3_h
#define pt3_h

namespace math
{
    template<typename T>
    struct pt3
    {
        T x;
        T y;
        T z;
        
        static pt3<T> make(T x, T y, T z)
        {
            pt3<T> result = {x, y, z};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        pt3<T> operator*(const T rhs) const
        {
            pt3<T> result = {x * rhs, y * rhs, z * rhs};
            return result;
        }
        
        pt3<T> operator/(const T rhs) const
        {
            pt3<T> result = {x / rhs, y / rhs, z / rhs};
            return result;
        }
    private:
        static T pt3<T>::*_elem[3];
    };
    
    template<typename T>
    T pt3<T>::*pt3<T>::_elem[3] = {&pt3<T>::x, &pt3<T>::y, &pt3<T>::z};
    
    typedef pt3<char> pt3c;
    typedef pt3<short> pt3s;
    typedef pt3<int> pt3i;
    typedef pt3<float> pt3f;
}

#endif
