#version 120

attribute vec2 position;
attribute vec2 texCoord;
varying vec2 outTexCoord;

void main()
{
	gl_Position =  gl_ModelViewProjectionMatrix * vec4(position, 0.0, 1.0);
    outTexCoord = texCoord;
}
