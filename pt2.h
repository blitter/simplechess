//
//  pt2.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef pt2_h
#define pt2_h

namespace math
{
    template<typename T>
    struct pt2
    {
        T x;
        T y;
        
        static pt2<T> make(T x, T y)
        {
            pt2<T> result = {x, y};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        pt2<T> operator*(const T rhs) const
        {
            pt2<T> result = {x * rhs, y * rhs};
            return result;
        }
        
        pt2<T> operator/(const T rhs) const
        {
            pt2<T> result = {x / rhs, y / rhs};
            return result;
        }
    private:
        static T pt2<T>::*_elem[2];
    };
    
    template<typename T>
    T pt2<T>::*pt2<T>::_elem[2] = {&pt2<T>::x, &pt2<T>::y};
    
    typedef pt2<char> pt2c;
    typedef pt2<short> pt2s;
    typedef pt2<int> pt2i;
    typedef pt2<float> pt2f;
}

#endif
