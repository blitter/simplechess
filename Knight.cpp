//
//  Knight.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Knight.h"

#include "Board.h"
#include "Piece.h"
#include "Tile.h"

void Knight::getValidMoves(Board& board, std::set<Tile*>& moveList) const
{
    moveList.clear();
    
    // "A Knight can move two horizontal and one vertical or two vertical and one horizontal in any direction (L)"
    const vec2i searchDirections[] =
    {
        {-1, -2},
        {1, -2},
        {2, -1},
        {2, 1},
        {1, 2},
        {-1, 2},
        {-2, 1},
        {-2, -1}
    };
    
    for (int i = 0; i < 8; ++i)
    {
        int thisX = getX() + searchDirections[i].x;
        int thisY = getY() + searchDirections[i].y;
        
        Tile* thisTile = board.getTile(thisX, thisY);
        if (thisTile != NULL)
        {
            const Piece* piece = thisTile->getPiece();
            if (piece != NULL)
            {
                if (piece->getColor() != getColor())
                {
                    // we can take an opponent but that terminates the move
                    moveList.insert(thisTile);
                }
            }
            else
            {
                moveList.insert(thisTile);
            }
        }
    }
}
