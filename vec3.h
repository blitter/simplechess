//
//  vec3.h
//
//  Created by Keith Kaisershot on 10/3/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef vec3_h
#define vec3_h

#include <math.h>

#include "pt3.h"

namespace math
{
    template<typename T>
    struct vec3
    {
        T x;
        T y;
        T z;
        
        static vec3<T> make(T x, T y, T z)
        {
            vec3<T> result = {x, y, z};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        vec3<T> operator-() const
        {
            vec3<T> result = {-x, -y, -z};
            return result;
        }
        
        void neg()
        {
            x = -x;
            y = -y;
            z = -z;
        }
        
        vec3<T> operator+(const vec3<T>& rhs) const
        {
            vec3<T> result = {x + rhs.x, y + rhs.y, z + rhs.z};
            return result;
        }
        
        vec3<T> operator-(const vec3<T>& rhs) const
        {
            vec3<T> result = {x - rhs.x, y - rhs.y, z - rhs.z};
            return result;
        }
        
        vec3<T> operator*(const T rhs) const
        {
            vec3<T> result = {x * rhs, y * rhs, z * rhs};
            return result;
        }
        
        vec3<T> operator/(const T rhs) const
        {
            vec3<T> result = {x / rhs, y / rhs, z / rhs};
            return result;
        }
        
        pt3<T> operator+(const pt3<T>& rhs) const
        {
            pt3<T> result = {x + rhs.x, y + rhs.y, z + rhs.z};
            return result;
        }
        
        pt3<T> operator-(const pt3<T>& rhs) const
        {
            pt3<T> result = {x - rhs.x, y - rhs.y, z - rhs.z};
            return result;
        }
        
        float dot(const vec3<T>& rhs) const
        {
            return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);
        }
        
        float lenSq() const
        {
            return dot(*this);
        }
        
        float len() const
        {
            return sqrtf(lenSq());
        }
        
        vec3<T> normalized() const
        {
            return *this / len();
        }
        
        void normalize()
        {
            *this /= len();
        }
        
        vec3<T> projOn(const vec3<T>& rhs) const
        {
            vec3<T> result = (dot(rhs) / rhs.lenSq()) * rhs;
            return result;
        }
        
        vec3<T> cross(const vec3<T>& rhs) const
        {
            vec3<T> result = {
                (y * rhs.z) - (z * rhs.y),
                (z * rhs.x) - (x * rhs.z),
                (x * rhs.y) - (y * rhs.x)
            };
            return result;
        }
    private:
        static T vec3<T>::*_elem[3];
    };
    
    template<typename T>
    T vec3<T>::*vec3<T>::_elem[3] = {&vec3<T>::x, &vec3<T>::y, &vec3<T>::z};
    
    typedef vec3<char> vec3c;
    typedef vec3<short> vec3s;
    typedef vec3<int> vec3i;
    typedef vec3<float> vec3f;
}

#endif
