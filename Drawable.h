//
//  Drawable.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Drawable_h
#define SimpleChess_Drawable_h

class Screen;

class Drawable
{
public:
    virtual ~Drawable(void) {};
    
    // drawing shouldn't mutate any state
    virtual void draw(const Screen& screen) const = 0;
    
};

#endif
