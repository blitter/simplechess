//
//  MouseMoveEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_MouseMoveEvent_h
#define SimpleChess_MouseMoveEvent_h

#include "MouseEvent.h"

class MouseMoveEvent : public MouseEvent
{
public:
    MouseMoveEvent(float x, float y) 
    : MouseEvent('move', x, y) {};
    ~MouseMoveEvent(void) {};
    
};

#endif
