#version 120

varying vec2 outTexCoord;
uniform sampler2D texture;
uniform float alphaFactor;

void main()
{
    vec4 texel = texture2D(texture, outTexCoord);
	gl_FragColor = vec4(texel.rgb, texel.a * alphaFactor);
}
