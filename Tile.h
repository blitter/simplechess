//
//  Tile.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Tile_h
#define SimpleChess_Tile_h

#include "GLEW/glew.h"

#include "Drawable.h"
#include "rgb.h"
#include "Updatable.h"

class Board;
class Piece;
class Shader;

using namespace geom;

class Tile : public Drawable, public Updatable
{
public:
    enum Color
    {
        BLACK,
        WHITE,
        NUM_COLORS
    };
    
    Tile(Color color);
    ~Tile(void);
    
    Color getColor(void) const { return _color; };
    
    Piece* getPiece(void) { return _piece; };
    void assignPiece(Piece* piece) { _piece = piece; };
    
    void setHilite(bool hilite);
    void setHiliteColor(rgbf color);
    
    void draw(const Screen& screen) const;
    
    void update(int deltaMS);
    
private:
    Color _color;
    Piece* _piece;
    
    bool _hiliting;
    int _hiliteTime;
    rgbf _hiliteRGB;
    rgbf _rgb;
    
    // we only need one vbo and shader for all tiles
    // we'll just translate them as needed
    // so keep a reference count of all tiles
    // and clean up the gl stuff when all tiles are gone
    static int _glRefCount;
    static Shader* _shader;
    static GLuint _vbo;
    
    rgbf getRGB(void) const;
    
};

#endif
