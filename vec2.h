//
//  vec2.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef vec2_h
#define vec2_h

#include <math.h>

#include "pt2.h"

namespace math
{
    template<typename T>
    struct vec2
    {
        T x;
        T y;
        
        static vec2<T> make(T x, T y)
        {
            vec2<T> result = {x, y};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        vec2<T> operator-() const
        {
            vec2<T> result = {-x, -y};
            return result;
        }
        
        void neg()
        {
            x = -x;
            y = -y;
        }
        
        vec2<T> operator+(const vec2<T>& rhs) const
        {
            vec2<T> result = {x + rhs.x, y + rhs.y};
            return result;
        }
        
        vec2<T> operator-(const vec2<T>& rhs) const
        {
            vec2<T> result = {x - rhs.x, y - rhs.y};
            return result;
        }
        
        vec2<T> operator*(const T rhs) const
        {
            vec2<T> result = {x * rhs, y * rhs};
            return result;
        }
        
        vec2<T> operator/(const T rhs) const
        {
            vec2<T> result = {x / rhs, y / rhs};
            return result;
        }
        
        pt2<T> operator+(const pt2<T>& rhs) const
        {
            pt2<T> result = {x + rhs.x, y + rhs.y};
            return result;
        }
        
        pt2<T> operator-(const pt2<T>& rhs) const
        {
            pt2<T> result = {x - rhs.x, y - rhs.y};
            return result;
        }
        
        float dot(const vec2<T>& rhs) const
        {
            return (x * rhs.x) + (y * rhs.y);
        }
        
        float lenSq() const
        {
            return dot(*this);
        }
        
        float len() const
        {
            return sqrtf(lenSq());
        }
        
        vec2<T> normalized() const
        {
            return *this / len();
        }
        
        void normalize()
        {
            *this /= len();
        }
        
        vec2<T> projOn(const vec2<T>& rhs) const
        {
            vec2<T> result = (dot(rhs) / rhs.lenSq()) * rhs;
            return result;
        }
    private:
        static T vec2<T>::*_elem[2];
    };
    
    template<typename T>
    T vec2<T>::*vec2<T>::_elem[2] = {&vec2<T>::x, &vec2<T>::y};
    
    typedef vec2<char> vec2c;
    typedef vec2<short> vec2s;
    typedef vec2<int> vec2i;
    typedef vec2<float> vec2f;
}

#endif
