//
//  ScoreEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_ScoreEvent_h
#define SimpleChess_ScoreEvent_h

#include "Event.h"

class ScoreEvent : public Event
{
public:
    ScoreEvent(int blackScore, int whiteScore) 
    : Event('scor')
    , _blackScore(blackScore)
    , _whiteScore(whiteScore) {};
    ~ScoreEvent(void) {};
    
    int blackScore(void) const { return _blackScore; };
    int whiteScore(void) const { return _whiteScore; };
    
private:
    int _blackScore;
    int _whiteScore;
    
};

#endif
