//
//  Bishop.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Bishop_h
#define SimpleChess_Bishop_h

#include "Piece.h"

class Bishop : public Piece
{
public:
    Bishop(Color color, Model& model)
    : Piece(color, model) {};
    ~Bishop(void) {};
    
    void getValidMoves(Board& board, std::set<Tile*>& moveList) const;

};

#endif
