//
//  QuitEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_QuitEvent_h
#define SimpleChess_QuitEvent_h

#include "Event.h"

class QuitEvent : public Event
{
public:
    QuitEvent(void) : Event('quit') {};
    ~QuitEvent(void) {};
    
};

#endif
