//
//  Piece.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Piece_h
#define SimpleChess_Piece_h

#include <set>

#include "GLEW/glew.h"

#include "Drawable.h"

class Board;
class Model;
class Shader;
class Tile;

class Piece : public Drawable
{
friend class Board;
public:
    enum Color
    {
        BLACK,
        WHITE,
        NUM_COLORS
    };
    
    Piece(Color color, Model& model);
    ~Piece(void);
    
    int getX(void) const { return _x; };
    int getY(void) const { return _y; };
    
    Color getColor(void) const { return _color; };
    
    Tile* getParent(void) const { return _parent; };
    virtual void getValidMoves(Board& board, std::set<Tile*>& moveList) const;
        
    void draw(const Screen& screen) const;
    
    void update(int deltaMS);

private:
    Color _color;
    Model& _model;
    
    Tile* _parent;
    
    int _x, _y;
    int _destX, _destY;
    float _offsetX, _offsetY;
        
    // we only need one shader for all pieces
    // so keep a reference count of all pieces
    // and clean up the shader when all pieces are gone
    static int _glRefCount;
    static Shader* _shader;
    
};

#endif
